# Due to check in
# ./meta/recipes-bsp/grub/grub2.inc
# <cut>
GRUB_COMPATIBLE_HOST = '(x86_64.*|i.86.*|arm.*|aarch64.*|riscv.*)-(linux.*|freebsd.*)'
COMPATIBLE_HOST = "${GRUB_COMPATIBLE_HOST}"
# Grub doesn't support hard float toolchain and won't be able to forcefully
# disable it on some of the target CPUs. See 'configure.ac' for
# supported/unsupported CPUs in hardfp.
#
#COMPATIBLE_HOST:armv7a = "${@'null' if bb.utils.contains('TUNE_CCARGS_MFLOAT', 'hard', True, False, d) else d.getVar('GRUB_COMPATIBLE_HOST')}"
#COMPATIBLE_HOST:armv7ve = "${@'null' if bb.utils.contains('TUNE_CCARGS_MFLOAT', 'hard', True, False, d) else d.getVar('GRUB_COMPATIBLE_HOST')}"
# </cut>
# recipe fails to compile. But actully grub-efi works with -mfloat-abi=hard
# Due to grub is temporary in meta-ledge-secure, keep it as workaround.
COMPATIBLE_HOST:armv7a = "${GRUB_COMPATIBLE_HOST}"
COMPATIBLE_HOST:armv7ve = "${GRUB_COMPATIBLE_HOST}"
