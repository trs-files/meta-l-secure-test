PV="23.01.00"
SRC_URI[sha256sum] = "d7fc306046852163149cc1572c4cea5f06ec6e725556943035c079cff0f125a8"
LIC_FILES_CHKSUM = "file://src/main.c;beginline=1;endline=16;md5=b0c6617190332a22f34a7b31222e0f7b"

# add arm-trs-linux-gnueabi
COMPATIBLE_HOST = "(i.86|x86_64|arm|aarch64|powerpc64).*-linux"
